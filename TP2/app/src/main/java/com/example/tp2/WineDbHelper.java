package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";
    private Context aContext;

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        aContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String creation = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME + " TEXT, " + COLUMN_WINE_REGION + " TEXT, " + COLUMN_LOC + " TEXT, " + COLUMN_CLIMATE + " TEXT, " + COLUMN_PLANTED_AREA + " TEXT)";
        db.execSQL(creation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(String.format("DROP IF TABLE EXISTS %s", TABLE_NAME));
        onCreate(db);
    }


    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues newWine = new ContentValues();
        newWine.put(COLUMN_NAME, wine.getTitle());
        newWine.put(COLUMN_WINE_REGION, wine.getRegion());
        newWine.put(COLUMN_LOC, wine.getLocalization());
        newWine.put(COLUMN_CLIMATE, wine.getClimate());
        newWine.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        Cursor cursor;
        String checkQuery = "SELECT " + COLUMN_NAME + " FROM " + TABLE_NAME + " WHERE " + COLUMN_NAME + " = '" + wine.getTitle() + "' AND "+ COLUMN_WINE_REGION + " = '" + wine.getRegion() + "'";
        cursor = db.rawQuery(checkQuery, null);
        if (cursor.getCount() == 0){
            cursor.close();
            long rowID = db.insert(TABLE_NAME, null, newWine);
            db.close();
            return (rowID != -1);
        } else {
            cursor.close();
            AlertDialog.Builder popup = new AlertDialog.Builder(aContext);
            popup.setTitle("Ajout Impossible");
            popup.setMessage("Un Vin portant le même nom et la même appélation existe déjà dans la base de données.");
            popup.show();
            return false;
        }
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
	    int res;
	    ContentValues value = new ContentValues();
	    value.put(COLUMN_WINE_REGION,wine.getRegion());
        value.put(COLUMN_LOC,wine.getLocalization());
        value.put(COLUMN_CLIMATE,wine.getClimate());
        value.put(COLUMN_PLANTED_AREA,wine.getPlantedArea());
	    String condi = COLUMN_NAME + " =?";
        String arg[] = {wine.getTitle()};
	    res = db.update(TABLE_NAME, value, condi, arg);
	    db.close();
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
	    Cursor cursor;
	    cursor = db.rawQuery(query, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

     public void deleteWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        String args[] = {wine.getTitle()};
        db.delete(TABLE_NAME, COLUMN_NAME + " =?", args);
        db.close();
    }

     public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = new Wine();
	    wine.setTitle(cursor.getString(1));
        wine.setRegion(cursor.getString(2));
        wine.setLocalization(cursor.getString(3));
        wine.setClimate(cursor.getString(4));
        wine.setPlantedArea(cursor.getString(5));
        return wine;
    }
}
