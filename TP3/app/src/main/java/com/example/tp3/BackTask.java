package com.example.tp3;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class BackTask extends AsyncTask<Team, Void, Team> {

    private Exception exception;
    private Context mContext;
    private int position;
    private Activity activity;

    public BackTask(Context mContext, int position, Activity activity){
        this.mContext = mContext;
        this.position = position;
        this.activity = activity;
    }

    protected Team doInBackground(Team... team) {
        WebServiceUrl web = new WebServiceUrl();
        URL url;
        HttpURLConnection connection = null;
        HttpURLConnection connection2 = null;
        HttpURLConnection connection3 = null;
        JSONResponseHandlerTeam js = new JSONResponseHandlerTeam(team[0]);
        try {
            url = web.buildSearchTeam(team[0].getName());
            connection = (HttpURLConnection) url.openConnection();
            InputStream input = connection.getInputStream();
            js.readJsonStream(input);
            URL url2 = web.buildGetRanking(js.getTeam().getIdLeague());
            connection2 = (HttpURLConnection) url2.openConnection();
            InputStream input2 = connection2.getInputStream();
            JSONResponseHandlerRanking js2 = new JSONResponseHandlerRanking(js.getTeam());
            js2.readJsonStream(input2);
            URL url3 = web.buildSearchLastEvents(js.getTeam().getIdTeam());
            connection3 = (HttpURLConnection) url3.openConnection();
            InputStream input3 = connection3.getInputStream();
            JSONResponseHandlerRanking js3 = new JSONResponseHandlerRanking(js2.getTeam());
            js3.readJsonStream(input3);
            return js3.getTeam();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            connection.disconnect();
            connection2.disconnect();
            connection3.disconnect();
        }
    }

    protected void onPostExecute(Team team) {
        SportDbHelper SDB = new SportDbHelper(mContext);
        SDB.updateTeam(team);
        TeamAdapter.getListe().set(position, team);
        TeamAdapter.get(mContext, SDB, activity).notifyDataSetChanged();
    }
}
