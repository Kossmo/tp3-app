package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    SportDbHelper SDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),AddTeam.class);
                startActivity(intent);
            }
        });
        initRecycleView();

        final SwipeRefreshLayout updateSwipe = findViewById(R.id.update);
        updateSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                for (int i = 0; i < TeamAdapter.getListe().size(); i++){
                    new BackTask(MainActivity.this, i, MainActivity.this).execute(TeamAdapter.getListe().get(i));
                    SportDbHelper SDB = new SportDbHelper(MainActivity.this);
                    TeamAdapter.get(MainActivity.this, SDB,MainActivity.this).notifyDataSetChanged();
                }
                updateSwipe.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initRecycleView(){
        RecyclerView recyclerView = findViewById(R.id.recyclerTeam);
        SDB = new SportDbHelper(this);
        Cursor cursor = SDB.fetchAllTeams();
        if (cursor.getCount() == 0){
            SDB.populate();
        }
        recyclerView.setAdapter(TeamAdapter.get(this, SDB,MainActivity.this));
        new ItemTouchHelper(itemCallCack).attachToRecyclerView(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    ItemTouchHelper.SimpleCallback itemCallCack = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            SDB.deleteTeam((int) TeamAdapter.getListe().get(viewHolder.getAdapterPosition()).getId());
            TeamAdapter.getListe().remove(viewHolder.getAdapterPosition());
            TeamAdapter.get(MainActivity.this, SDB, MainActivity.this).notifyDataSetChanged();
        }
    };
}
